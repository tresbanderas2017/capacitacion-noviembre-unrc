import express from 'express' //express es un framework de node.js
import api from './api'  //por defecto me traera el index.js

const app = express() //además de var, se pueden declarar las variables como let y const

app.use('/api',api()) //concateno las rutas de api 

app.get('/hola/:nombre', (req,res)=> {//req: request y res: resultado
			const nombre = req.params.nombre
			res.send(`<h1>Hola ${nombre}</h1>`)//funcion sin nombre, con dos parametros
		}
	)

app.get('/', (req,res)=> //req: request y res: resultado
		res.send("<h1>Hola don pepito</h1>")//funcion sin nombre, con dos parametros y de una línea (sin llaves)
	)


app.listen(//listen tiene dos parametros
					8080, //param1: puerto
					()=>console.log("servidor corriendo che") //param2: funcion sin nombre, sin parametros y sin llaves (xq es de una linea)
)
