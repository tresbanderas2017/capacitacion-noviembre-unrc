import {Router} from 'express' //importo solo el módulo Router del framework express

const personas = [
  {
    nombre: 'Javier',
    equipo: 'River Plate',
    telefono: 1099,
    id: 0
  },
  {
    nombre: 'Sebastian',
    equipo: 'Rampla Juniors',
    telefono: 298,
    id: 1
  },
  {
    nombre: 'Maria',
    equipo: 'Boca Juniors',
    telefono: 3000,
    id:2
  },
  {
    nombre: 'Julián',
    equipo: 'Newells Old Boys',
    telefono: 4234,
    id:3
  }
]

export default ()=> {
  const api = Router()
  api.get('/personas/:idSearch', (req,res)=> {
                                  const id = req.params.idSearch
                                  const persona = personas.find(p => p.id === parseInt(id))
                                  //para testear hago los console
                                  console.log(id)
                                  console.log(persona)
                                  res.json(persona)
                                })


  api.get('/personas', (req,res)=> {
                                  //console.log("haciendo api"); //si llamaba a esto quedaba esperando una respuesta http
                                  // res.json({equipo:'Atlanta de Vedia'}) // primer ejemplo
                                  res.json(personas) // segundo ejemplo: retorna un endpoints que es un array de json
                                  })
  return api
}
